﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace TextFileViewer {
    public partial class MainForm : Form {
        //Keep track of whether the currently open file contains a utf-8 BOM
        private bool bHasBOM = false;
        private string strCurrentFilePath = "";
        

        public MainForm() {
            InitializeComponent();
        }

        private void miQuit_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void miOpen_Click(object sender, EventArgs e) {
            DialogResult drOpenResult = ofdOpenFile.ShowDialog();
            //only proceed if the dialog result is "OK"
            if (drOpenResult == DialogResult.OK) {
                //get the path of the file selected by the user and open it.
                strCurrentFilePath = ofdOpenFile.FileName;
                FileStream fsFile = File.Open(strCurrentFilePath, FileMode.Open, FileAccess.Read);
                //Read the data in the file, convert to text, and display in the rich text box.
                //Set up buffer to hold data read from file.
                const int iBufferSize = 4096;
                byte[] byBuffer = new byte[iBufferSize];
                //clear any text that currently appears in the rich text box
                rtbText.Clear();
                //Initial read to start the process.Check for UTF-8 BOM at beginning of file
                bHasBOM = false;
                string strNewText = "";
                int iBytesRead = fsFile.Read(byBuffer, 0, iBufferSize);
                if (iBytesRead > 0 && byBuffer[0] == 0xEF) {
                    if (iBytesRead > 0 && byBuffer[1] == 0xBB) {
                        if (iBytesRead > 0 && byBuffer[2] == 0xBF) {
                            //there is a utf-8 BOM at the beggining. Keep track of this, then read first byte of text to start loop
                            bHasBOM = true;
                            strNewText = Encoding.ASCII.GetString(byBuffer, 3, iBytesRead-3);
                            rtbText.AppendText(strNewText);
                            iBytesRead = fsFile.Read(byBuffer, 0, iBufferSize);
                        }
                    }
                }
                while (iBytesRead > 0) {
                    //Convert the bytes into a string of chars and append to the rich text box
                    strNewText = Encoding.ASCII.GetString(byBuffer, 0, iBytesRead);
                    rtbText.AppendText(strNewText);
                    //Read a new block of bytes from the file
                    iBytesRead = fsFile.Read(byBuffer, 0, iBufferSize);
                }
                //Finished. Close the file
                fsFile.Close();
            }
        }

        private void miSave_Click(object sender, EventArgs e) {
            sfdSaveFile.FileName = strCurrentFilePath;
            DialogResult drSaveFile = sfdSaveFile.ShowDialog();
            if(drSaveFile == DialogResult.OK) {
                strCurrentFilePath = sfdSaveFile.FileName;
                //used to create or rewrite a file
                FileStream fsSaveFile = File.Open(strCurrentFilePath, FileMode.Create, FileAccess.Write);
                if (bHasBOM) {
                    byte[] byBOM = new byte[3] { 0xEF, 0xBB, 0xBF };
                    fsSaveFile.Write(byBOM, 0, byBOM.Length);
                }
                //changes the text into bytes
                byte[] byBuffer = Encoding.ASCII.GetBytes(rtbText.Text);
                fsSaveFile.Write(byBuffer, 0, byBuffer.Length);
                fsSaveFile.Close();
            }
        }
    }
}
